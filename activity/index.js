// console.log("Hello World!");

// 3. Insert a single room (insertOne method)

db.rooms.insert({
  name: "double",
  accommodate: 2,
  price: 1000,
  description: "A simple room with all the basic necessities",
  roomsAvailable: 10,
  isAvailable: false,
});

// 4. Insert multiple rooms (insertMany method) 

db.rooms.insertMany([
  {
      name: "double",
      accommodate: 3,
      price: 2000,
      description: "A room fit for small family going on a vacation",
      roomsAvailable: 5,
      isAvailable: false    
  },
  {
      accommodate: 4,
      price: 4000,
      description: "A room with a queen-sized bed perfect for a simple getaway",
      roomsAvailable: 15,
      isAvailable: false
  }
  ]);

// 5. Use the find method to search for a room with the name double

db.rooms.find({
  name: "double"
});

// 6. Use the updateOne method to update the queen room and set the available rooms to 0

db.rooms.updateOne(
    {price: 4000},
    {
        $set: {
            roomsAvailable: 0,
            name: "queen"
            }
        }
);

// 7. Use the deleteMany method rooms to delete all rooms that have 0 availability

db.rooms.deleteMany({
  roomsAvailable: 0
});
